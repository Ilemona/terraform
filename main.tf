
provider "aws" {
  region = var.avail_zone

}


variable "vpc_cidr_block" {
  description = "Enter it in the var session"
}

variable "avail_zone" {
  
}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "development"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = "10.0.10.0/24"
  availability_zone = "us-east-1a"
}


data "aws_vpc" "existing-vpc" {
    default = true
  
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id = data.aws_vpc.existing-vpc.id
  cidr_block = var.subnet_cidr_block #"172.31.96.0/20"
  availability_zone = "us-east-1a"
}

output "dev-vpc-id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-2.id
}

variable "subnet_cidr_block" {
    description = "Input CIDR block Mona"
  
}